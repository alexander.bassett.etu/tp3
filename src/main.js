import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

import Component from './components/Component';

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche la liste des pizzas
Router.menuElement = document.querySelector('.mainMenu');

document.querySelector('.logo').innerHTML =
	document.querySelector('.logo').innerHTML +
	"<small>les pizzas c'est la vie</small>";

let currentClass = document
	.querySelectorAll('nav a[href]')[1]
	.getAttribute('class');
document
	.querySelectorAll('nav a[href]')[1]
	.setAttribute('class', currentClass + ' active');

let newsContainer = document.querySelector('.newsContainer');
newsContainer.style.display = '';

//console.log(document.querySelector('.newsContainer'));

document
	.querySelector('.newsContainer .closeButton')
	.addEventListener('click', event => {
		event.preventDefault();
		newsContainer.style.display = 'none';
	});
